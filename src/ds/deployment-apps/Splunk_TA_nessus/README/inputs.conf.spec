[nessus://<name>]
index = <value>
disabled = <value>
interval = <value>

url = <value>
access_key = <value>
secret_key = <value>
start_date = <value>

# The metrics TA can collect. Should be nessus_scan or nessus_plugin
metric = <value>

# The number of individual events that are returned in each REST request.
page_size = <value>

# The batch size of events collected for each interval.
batch_size = <value>