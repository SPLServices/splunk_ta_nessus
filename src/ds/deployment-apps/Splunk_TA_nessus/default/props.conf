## Apply the following properties to nessus text files (.nessus)
[source::....nessus]
sourcetype = nessus

[nessus]
## Index w/ current time
DATETIME_CONFIG = CURRENT
## Index w/ ReportHost StopTime
## - Uncomment the following settings to enable use of end_time as the
##   indexing time. Note that the XML-formatted Nessus reports must include a 
##   HOST_END field to use this setting.
#DATETIME_CONFIG =
#TIME_PREFIX = end_time="
#TIME_FORMAT = %a %b %d %H:%M:%S %Y

SHOULD_LINEMERGE = false
LINE_BREAKER = ([\r\n]+---splunk-ta-nessus-end-of-event---[\r\n]+)

KV_MODE = none
REPORT-0auto_kv_for_nessus = auto_kv_for_nessus
REPORT-dest_for_nessus = dest_dns_as_dest,dest_nt_host_as_dest,dest_ip_as_dest,dest_mac_as_dest
REPORT-dest_port_for_nessus = dest_port-transport_for_nessus
LOOKUP-severity_for_nessus = nessus_severity_lookup severity_id OUTPUT severity
LOOKUP-vendor_info_for_nessus = nessus_vendor_info_lookup sourcetype OUTPUT vendor,product

[nessus:plugin]
TZ = UTC
KV_MODE = json

REPORT-nessus_mskb_from_see_also = nessus_mskb_from_see_also
FIELDALIAS-nessus_bugtraq = bid{} AS bugtraq
FIELDALIAS-nessus_signature = id AS signature_id plugin_name AS signature
FIELDALIAS-nessus_cvss = cvss_base_score AS cvss

FIELDALIAS-cve = cve{} AS cve
FIELDALIAS-cpe = cpe{} AS cpe
FIELDALIAS-osvdb = osvdb{} AS osvdb
FIELDALIAS-cert = cert{} AS cert
FIELDALIAS-xref = xref{} AS xref
FIELDALIAS-msft = msft{} AS msft
FIELDALIAS-see_also = see_also{} AS see_also

LOOKUP-vendor_info_for_nessus = nessus_vendor_info_lookup sourcetype OUTPUT vendor,product

[nessus:scan]
TZ = UTC
KV_MODE = json

FIELDALIAS-nessus_dest_ip = host-ip AS dest_ip
FIELDALIAS-nessus_dest = hostname AS dest_host
FIELDALIAS-nessus_severity = severity AS severity_id

EVAL-dest = coalesce(dest_ip, dest_host)
EVAL-dvc = if((scanner_name=="Local Scanner"),host,scanner_name)

LOOKUP-severity_for_nessus = nessus_severity_lookup severity_id OUTPUT severity
LOOKUP-vendor_info_for_nessus = nessus_vendor_info_lookup sourcetype OUTPUT vendor,product
LOOKUP-plugin_for_nessus_kb = nessus_plugin_lookup id AS plugin_id
LOOKUP-plugin_for_nessus_kb_mv_cve = nessus_mv_cve_lookup id AS plugin_id OUTPUT cve
LOOKUP-plugin_for_nessus_kb_mv_cpe = nessus_mv_cpe_lookup id AS plugin_id OUTPUT cpe
LOOKUP-plugin_for_nessus_kb_mv_bugtraq = nessus_mv_bugtraq_lookup id AS plugin_id OUTPUT bugtraq
LOOKUP-plugin_for_nessus_kb_mv_osvdb = nessus_mv_osvdb_lookup id AS plugin_id OUTPUT osvdb
LOOKUP-plugin_for_nessus_kb_mv_xref = nessus_mv_xref_lookup id AS plugin_id OUTPUT xref
LOOKUP-plugin_for_nessus_kb_mv_msft = nessus_mv_msft_lookup id AS plugin_id OUTPUT msft
LOOKUP-plugin_for_nessus_kb_mv_mskb = nessus_mv_mskb_lookup id AS plugin_id OUTPUT mskb
LOOKUP-plugin_for_nessus_kb_mv_cert = nessus_mv_cert_lookup id AS plugin_id OUTPUT cert
LOOKUP-plugin_for_nessus_kb_mv_see_also = nessus_mv_see_also_lookup id AS plugin_id OUTPUT see_also


[source::...ta_nessus.log*]
sourcetype = ta:nessus:log
