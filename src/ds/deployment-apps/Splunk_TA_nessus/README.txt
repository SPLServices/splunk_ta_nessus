Splunk Add-on for Nessus version 4.0.0

Copyright (C) 2009-2015 Splunk Inc. All Rights Reserved.
 
The Splunk Add-on for Nessus allows a Splunk software administrator to collect Nessus vulnerability scan reports. You can then directly analyze the Nessus data or use it as a contextual data feed to correlate with other vulnerability related data in the Splunk platform. This add-on provides the inputs and CIM-compatible knowledge to use the add-on with other Splunk apps, such as Splunk Enterprise Security and the Splunk App for PCI Compliance.

Documentation for this add-on is located at: http://docs.splunk.com/Documentation/AddOns/latest/Nessus/Description

For installation and set-up instructions, refer to the Installation and Configuration section: http://docs.splunk.com/Documentation/AddOns/latest/Nessus/Hardwareandsoftwarerequirements

For release notes, refer to the Release notes section: http://docs.splunk.com/Documentation/AddOns/latest/Nessus/Releasenotes