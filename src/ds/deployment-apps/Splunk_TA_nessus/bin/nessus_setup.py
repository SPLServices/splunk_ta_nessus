import splunk.admin as admin
import splunk.clilib.cli_common as scc
from splunktalib import credentials as cred
from splunktalib.conf_manager import conf_manager as conf
from splunktalib.common import log
from splunktalib.common import util
import logging
import traceback

logger = log.Logs().get_logger('ta_nessus', level=logging.DEBUG)

util.remove_http_proxy_env_vars()

"""
Copyright (C) 2005 - 2015 Splunk Inc. All Rights Reserved.
Description:  This skeleton python script handles the parameters in the configuration page.

      handleList method: lists configurable parameters in the configuration page
      corresponds to handleractions = list in restmap.conf

      handleEdit method: controls the parameters and saves the values
      corresponds to handleractions = edit in restmap.conf
"""

class ConfigApp(admin.MConfigHandler):

    encrypted = "********"

    """
    Set up supported arguments
    """
    def setup(self):
        if self.requestedAction == admin.ACTION_EDIT:
            for arg in ['proxy_enabled', 'proxy_type', 'proxy_url', 'proxy_port',
                'proxy_username', 'proxy_password', 'proxy_rdns', 'loglevel']:
                self.supportedArgs.addOptArg(arg)

    """
    Read the initial values of the parameters from the custom file
      nessus.conf, and write them to the setup screen.

    If the app has never been set up,
      uses .../<appname>/default/nessus.conf.

    If app has been set up, looks at
      .../local/nessus.conf first, then looks at
    .../default/nessus.conf only if there is no value for a field in
      .../local/nessus.conf

    For boolean fields, may need to switch the true/false setting.

    For text fields, if the conf file says None, set to the empty string.

    """

    def handleList(self, confInfo):
        confDict = self.readConf("nessus")
        if confDict is not None:
            self._decrypt_username_password(confDict)
            proxy = confDict.get('nessus_proxy')
            assert proxy is not None, "nessus proxy file doesn't contain 'proxy stanza' inside"

            for key, val in proxy.items():
                if key == 'proxy_password':
                    val = ''
                confInfo['nessus_proxy'].append(key, val)

            confInfo["nessus_proxy"].append('loglevel', \
                confDict.get('nessus_loglevel', {}).get('loglevel', "WARN"))

    """
    After user clicks Save on setup screen, take updated parameters,
    normalize them, and save them somewhere
    """
    def handleEdit(self, confInfo):
        args = self.callerArgs.data
        for key, val in args.items():
            if val[0] is None:
                val[0] = ''

        proxy_stanza = {}
        proxy_enabled = args['proxy_enabled'][0]
        proxy_stanza['proxy_enabled'] = proxy_enabled
        loglevel_stanza = {}
        loglevel = args['loglevel'][0]
        loglevel_stanza['loglevel'] = loglevel
        if proxy_enabled.lower().strip() in ("1", "true", "yes", "t", "y"):
            proxy_port = args['proxy_port'][0]
            proxy_url = args['proxy_url'][0]
            proxy_type = args['proxy_type'][0]
            proxy_rdns = args['proxy_rdns'][0]
            # Validate args
            if proxy_url.lower().startswith("http:") or proxy_url.lower().startswith("https:"):
                raise admin.ArgValidationException("Proxy url should not start with http: or https:")
                
            if proxy_url != '' and proxy_port == '':
                raise admin.ArgValidationException("Port should not be blank")

            if proxy_url == '' and proxy_port != '':
                raise admin.ArgValidationException("URL should not be blank")

            if proxy_port != '' and not proxy_port.isdigit():
                raise admin.ArgValidationException("Port should be digit")

            # Proxy is enabled, but proxy url or port is empty
            if proxy_url == '' or proxy_port == '':
                raise admin.ArgValidationException("URL and port should not be blank")

            # Password is filled but username is empty
            if args['proxy_password'][0] != '' and args['proxy_username'][0] == '':
                raise admin.ArgValidationException("Username should not be blank")

            if proxy_type not in ('http', 'http_no_tunnel', 'socks4', 'socks5'):
                raise admin.ArgValidationException("Unsupported proxy type")

            confDict = self.readConf("nessus")
            self._decrypt_username_password(confDict)
            proxy = confDict['nessus_proxy']

            proxy_stanza['proxy_url'] = proxy_url
            proxy_stanza['proxy_port'] = proxy_port
            proxy_stanza['proxy_type'] = proxy_type
            proxy_stanza['proxy_rdns'] = proxy_rdns

            cred_mgr = cred.CredentialManager(session_key=self.getSessionKey(),
                        app=self.appName, splunkd_uri=scc.getMgmtUri())
            if not args['proxy_username'][0]:
                try:
                    result = cred_mgr.delete('__Splunk_TA_nessus_proxy__')
                    logger.info("Update result:"+str(result))
                except Exception:
                    logger.warn("ERROR in deleting cred stanza")
                    logger.warn(traceback.format_exc())
            elif args['proxy_password'][0] != '' or \
                    proxy['proxy_username'] != args['proxy_username'][0]:
                stanza = {}
                proxy = {'proxy_username': args['proxy_username'][0],
                            'proxy_password': args['proxy_password'][0]}
                stanza['__Splunk_TA_nessus_proxy__'] = proxy

                try:
                    result = cred_mgr.update(stanza)
                    logger.info("Update result:"+str(result))
                except Exception:
                    logger.error("ERROR in creating cred stanza")
                    logger.error(traceback.format_exc())
                    raise admin.HandlerSetupException(
                            "fail to encrypt proxy username and password.")

            if args['proxy_password'][0] != '' or args['proxy_username'][0] != '':
                proxy_stanza['proxy_username'] = self.encrypted
                proxy_stanza['proxy_password'] = self.encrypted
            else:
                proxy_stanza['proxy_username'] = ''
                proxy_stanza['proxy_password'] = ''

        conf_mgr = conf.ConfManager(splunkd_uri=scc.getMgmtUri(), session_key=self.getSessionKey(),
                                    app_name=self.appName, owner='-')
        success = conf_mgr.update_stanza('nessus', 'nessus_proxy', proxy_stanza) \
                    and conf_mgr.update_stanza('nessus','nessus_loglevel',loglevel_stanza)

        if not success:
            logger.error("ERROR in writing nessus conf file.")
            raise admin.HandlerSetupException("fail to store data into nessus.conf file.")

    def _decrypt_username_password(self, confDict):
        try:
            if confDict.get("nessus_proxy") is not None:
                account = confDict["nessus_proxy"]
                is_encrypted = all(account.get(k, None) == \
                    self.encrypted for k in ("proxy_username", "proxy_password"))
                if is_encrypted:
                    cred_mgr = cred.CredentialManager(session_key=self.getSessionKey(),
                        app=self.appName, splunkd_uri=scc.getMgmtUri())
                    user_creds = cred_mgr.get_clear_password("__Splunk_TA_nessus_proxy__")
                    account["proxy_username"] = user_creds["__Splunk_TA_nessus_proxy__"]["proxy_username"]
                    account["proxy_password"] = user_creds["__Splunk_TA_nessus_proxy__"]["proxy_password"]
        except Exception:
            logger.error("decryption error. fail to decrypt encrypted proxy username/pwd. ")
            logger.error(traceback.format_exc())

# initialize the handler
admin.init(ConfigApp, admin.CONTEXT_NONE)
